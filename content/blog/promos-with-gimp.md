---
title: "Developing your own prize wall"
date: 2018-11-24T20:44:17-06:00
image: /images/blog/blog-promos.jpg
---

Pokemon and Magic rule our local scene, with Tuesday night Pokemon easily drawing 40 people throughout the night. I take my daughter up for a few games when things aren't too crazy, and she tears it up with her deck of the week. At the end of the night, we get to her favorite part: stamping her participation sheet. 

At 10 stamps, she gets to pick a special promo from their prize book. The promos aren't anything crazy (usually holographic versions of common creatures), but everyone in that room looks forward to when the new options are released. There is a promo for every faction, so everyone has something to shoot for.

It's not just the young kids that get excited, people of all ages are grinding for the newest promos, especially when all they have to do is play a game they already love.

> It doesn't matter if she won the game, just that she showed up and participated in the community. 

We don't have Pokemon level support yet, and that's ok. In order to get there, we have to survive, and that means doing some stuff on our own for a bit. For our local scene, we have 10-12 regular players, and a quarterly kit usually covers us for one night, with maybe a few of the leftover commons thrown around as prizes for the next couple weeks. Locally, we'd love to have a first-party league kit, where we all chip in 5 bucks, and that covers us for the entire 12-16 week cycle between sets. Having researched printing promos, I think you can easily support a 20 person community for around 50 bucks, while still giving folks a chance to earn a promo card every other week.

Notice that I said earn, not win. Winning is great, but it's not how you get the average player to come back every week. 

Your local Destiny scene is full of different types of players: competitive players who love to test and define the meta, tinkerers who are looking for the next broken thing, and of course, the casual player. The casual player may not walk out with that beautiful Ahsoka promo from a store championship, but that doesn't mean they don't want something cool of their own.

We're taking a crack at doing something for everyone by building our own prize wall. Here's our plan for content.

> Print custom promos that are interesting to new players

For us, that means focusing on the iconic characters that can be found in the starters packs. New players should be able to get a promo they can use immediately (Phasma, Rey, Rivals Anakin, etc). Almost all of these characters are playable at a tier 2 level, with several creeping even higher at times. We're right on the cusp of a new cycle and new starters, so we're anxiously awaiting card specs from the next set so we can have them available at launch.

> Focus on the new stuff that your community is interested in.

Across the Galaxy dropped a couple of weeks ago, and Han, Qui-Gon, and of course Vader are starting to show up. A casual player is going to pick up a couple of packs at a time, and that means they aren't going to be showing up with elite Vader unless they get really lucky, make some good trades, or suddently go off the deep end on the chase. 

Of course we made promos for the iconic characters in this next set (which were unfortunately mostly legendaries), but we also picked out some of the key rares that people like: Qi'ra, Leia, and Savage.

Feel free to poll the audience, and see what they are chasing. We love our laser swords, so Blue is usually a safe bet, but we also have folks who are crazy about Rebels and Clone Wars, so Rex and Greivous have made appearances too.

> Don't forget the competitive folks

When we did our print run, we did a couple of smaller blocks of alternative designs for the top players in our occasional swiss event. Many of us will play in regionals, and we expect to spend a decent amount of time practicing gauntlets. It's important to have stuff for your competitive players, but Organized Play is a time for everyone to have fun, not just the gauntlet grinders.

> Don't let them become an e-bay thing

It's great that there are people making cool swag on the Internet. Let those people do their thing while you make cool stuff for your friends. Ask if you can use your local store logo (or make a custom logo for your group), and slap it on your cards in a way that prevents them from becoming e-bay fodder.

----

We hope these ideas were helpful for you, and we'll follow up with a couple more articles soon including how we do points for promos, and how to design your own promos using GIMP, a free, cross-platform alternative to Photoshop.

Have fun this week!
