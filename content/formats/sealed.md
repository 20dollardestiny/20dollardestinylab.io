---
title: "Sealed League"
author: Team
date: 2018-11-24T14:51:12+06:00
image: /images/blog/blog-sealed.jpg
---

Everyone needs to start their collection somewhere, and the release of a new set is a great time to start. Excitement is high for new cards, spoilers have been flowing, and new players have a chance to come in at the ground floor.

With Star Wars Destiny, this happens every three months for a new set, and every year for a new cycle. New cycles are the ideal opportunity for a new player to join because they include starter decks, but a new set can be just as good.

> An even playing field is essential to a good playing experience.

Everyone should start the sealed league with a pre-defined set of cards. We use Rivals locally, but you could just as easily let players choose one of the new starters like Luke / Han or Boba / Trooper if you need something spicier than Rivals.

*Week 1:*

Everyone buys 8 packs. Ask your store if you can get a good price for the event. Around $20 is perfect. Everyone cracks their packs and build the best deck they can following normal sealed rules.

*Week 2:*

Everyone can add 3 more packs. Trades are encouraged; however you may need to implement some limits on cards. No one wants to play against a deck with 9 Ammo Reserves. Play some games!

*Week 3:*

It's time for the finals. Add your last 3 more packs, make your final trades, and get those games in.


 


