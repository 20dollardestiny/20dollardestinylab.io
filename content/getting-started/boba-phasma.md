---
title: "My First Deck: Look into my Helmet"
date: 2018-11-25T20:44:17-06:00
image: /images/blog/boba-phasma.jpg
---

If ever a costume could sell a character, it would be these two. While they may not have much substance in the movies, they certainly pack a punch in Destiny.

----

![alt text](/images/blog/blog-boba-phasma.png "Boba Phasma Deck Image")

---

The premise of this deck is simple, put guns on your characters, and hit as hard as you can. Leverage cards like [Quick Draw](https://swdestinydb.com/card/05155), [Tactical Mastery](https://swdestinydb.com/card/04015), and [Grand Entrance](https://swdestinydb.com/card/07122) to try and get those big unmitigatable rolls, and then time claiming the battlefield for maximum effect.

In fact, this deck really needs to go fast, because at only 21 health you aren't going to hanging out at the party for long. Claiming the battlefield also gets you the ability to use Hasty Exit, and ensures that you are shooting first each round.