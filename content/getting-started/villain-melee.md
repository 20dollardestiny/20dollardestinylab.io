---
title: "My First Deck: Massive Melee"
date: 2018-11-24T20:44:17-06:00
image: /images/blog/blog-savage.jpg
---

Savage Opress is a melee monster, and a recognizable face from The Clone Wars that is perfect for new players who love to swing big. 

With 16 points leftover, it's time to find him some stabby friends.

----

![alt text](/images/blog/blog-budget-melee.png "Savage Executioners Deck Image")

---

> Savage looks broke! And short on cash too!

Savage's lack of resource sides is going to be a problem, but in standard, you've got [Enrage](https://swdestinydb.com/card/01081) to soak some of Savage's health in exchange for a buck. If you don't have it, or need a Trilogies-compatible alternative, grab [Torment](https://swdestinydb.com/card/07017).

[It Binds All Things](https://swdestinydb.com/card/01150) will be another card you would be happy to see in your opening hand, so you can start off your game with some cheaper toys.

Couple all that with cheap removal, and you'll be just fine.

> Um, wait, Savage has to go first?

Just own it. [Bloodlust](https://swdestinydb.com/card/08014) can be played after Savage is activated, and you still get its benefits. You can also play [Crafted Lightsaber](https://swdestinydb.com/card/06006) without feeling bad, and that card has amazing sides.

If Savage makes it to the late game, you want to own that battlefield or there is no way you're going to get to dodge an opponent's blowout roll.